<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 19.10.18
 * Time: 21:43
 */

class Post
{

    private $forPage = 5;

    /**
     * @return array
     */
    public static function getAllPosts($page)
    {
        $db = Db::getConnection();
        $postModel = new Post();
        $result['popular_post'] = $postModel->getPopularPosts($db);
        $result['paginate'] = $postModel->paginateForOtherPosts($db, $page);
        $result['other_post'] = $postModel->getOtherPosts($db, $result['paginate']['page'], $result['paginate']['start']);
//        foreach ($result as $item) {
//            if (!$item) {
//                return false;
//            }
//        }
        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getPostById($id)
    {
        $db = Db::getConnection();
        $postModel = new Post();
        $result['comments'] = $postModel->getCommentsByPostId($id, $db);
        $post = $db->query('SELECT * FROM posts WHERE id=' . $id);
        if ($post) {
            $result['post'] = $post->fetch();
            return $result;
        }
        return false;


    }

    public function getCommentsByPostId($id, $db)
    {
        $comments = $db->query('SELECT c.id, c.author_comment, c.message, c.date  FROM posts p INNER JOIN comments c ON p.id = c.post_id WHERE p.id =' . $id . ' ORDER BY c.date desc ');
        if ($comments) {
            $comments->setFetchMode(PDO::FETCH_ASSOC);
            return $result = $comments->fetchAll();
        }
        return false;

    }

    public function getPopularPosts($db)
    {
        $result = $db->query('SELECT p.id, p.title, p.text, p.author_name, p.date, COUNT(c.id) com FROM posts p LEFT JOIN comments c ON p.id = c.post_id  GROUP BY p.id ORDER BY com desc LIMIT 5');
        if (!$result) {
            return false;
        }
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $popPosts = $result->fetchAll();
        foreach ($popPosts as $key => $post) {
            $shortText = substr($post['text'], 0, 100);
            $popPosts[$key]['short_text'] = $shortText;
        }
        return $popPosts;
    }

    public function getOtherPosts($db, $page, $start)
    {

        $otherPosts = $db->query(
            'SELECT p.id, p.title, p.text, p.author_name, p.date, COUNT(c.id) com 
                      FROM posts p 
                      LEFT JOIN comments c ON p.id = c.post_id 
                      GROUP BY p.id 
                      ORDER BY p.date desc LIMIT ' . $start . ',' . $this->forPage);
        if ($otherPosts) {
            $otherPosts->setFetchMode(PDO::FETCH_ASSOC);
            $otherPosts = $otherPosts->fetchAll();
            foreach ($otherPosts as $key => $post) {
                $shortText = substr($post['text'], 0, 100);
                $otherPosts[$key]['short_text'] = $shortText;
            }
            return $otherPosts;
        } else {
            return false;
        }

    }

    public function paginateForOtherPosts($db, $page)
    {
        $start = $page * $this->forPage - $this->forPage;
        $totalCount = $db->query('SELECT COUNT(*) co FROM posts');
        if (!$totalCount) {
            return false;
        }
        $totalCount->setFetchMode(PDO::FETCH_ASSOC);
        $totalCount = $totalCount->fetch()['co'];
        $countPage = $totalCount / $this->forPage;
//        $page = intval($countPage);


        if (empty($page) or $page < 0)
            $page = 1;

        if ($page > $totalCount)


            $result['next_page'] = false;
        $result['page'] = $page;
        $result['total_page'] = $countPage;
        $result['prev_page'] = $page - 1;
        if ($page == 1) {
            $result['prev_page'] = false;
        }
        $result['next_page'] = $page + 1;
        if ($page >= $countPage) {
            $result['next_page'] = false;
        }
        $result['start'] = $start;
        return $result;
    }

    public static function createNewPost($author, $title, $text)
    {
        $db = Db::getConnection();
        $stmt = $db->prepare("INSERT INTO posts (title, text,author_name) VALUES (:title, :text, :author_name)");
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':author_name', $author);
        $stmt->execute();

    }

    public static function saveComment($message, $authorName, $postId)
    {
        $db = Db::getConnection();
        $stmt = $db->prepare("INSERT INTO comments (message, author_comment, post_id) VALUES (:message, :author_comment, :post_id)");
        $stmt->bindParam(':message', $message);
        $stmt->bindParam(':author_comment', $authorName);
        $stmt->bindParam(':post_id', $postId);
        $stmt->execute();
    }
}