<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 17.10.18
 * Time: 23:17
 */
return array(
    'post-create' => 'post/store',
    'post/save-comment' => 'post/comment',
    'posts/page=([0-9]+)' => 'post/index/$1',
    'post/([0-9]+)' => 'post/show/$1',
    'posts' => 'post/index',
//    '' => 'post/index',



);