<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 19.10.18
 * Time: 21:43
 */

class Db
{

    /**
     * @return PDO
     */
    public static function getConnection() :PDO
    {
        $paramsPath = ROOT . '/config/db_params.php';
        $params = include($paramsPath);
        $dsn = "mysql:host={$params['host']}; dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);
        return $db;
    }
}