<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 17.10.18
 * Time: 23:11
 */

class Router
{
    /**
     * @var mixed
     */
    private $routes;


    /**
     * Router constructor.
     */
    public function __construct()
    {
        $routerPath = ROOT . '/config/routes.php';
        $this->routes = include($routerPath);
    }

    /**
     * @return string
     */
    private function getUri()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     *
     */
    public function run()
    {
            $uri = $this->getUri();
            if ($uri == '') {
                include_once (ROOT.'/controllers/PostController.php');
                $controller = new PostController();
                $controller->actionIndex();

            }
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~\b$uriPattern\b~", $uri)) {
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode('/', $internalRoute);
//                print_r($internalRoute);
//                break;
                $controllerName = ucfirst(array_shift($segments) . 'Controller');
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                    $controllerObject = new $controllerName();
                        $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                    if ($result == null) {
                        break;
                    }


                }
            }


        }
    }


}