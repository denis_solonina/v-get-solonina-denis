<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 17.10.18
 * Time: 22:58
 */

define('ROOT', dirname(__FILE__));
require_once (ROOT.'/components/Router.php');
require_once (ROOT.'/components/Db.php');
$router = new Router();
$router->run();
