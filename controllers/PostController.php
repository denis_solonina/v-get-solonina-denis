<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 18.10.18
 * Time: 22:24
 */
include_once ROOT . '/models/Post.php';

class PostController
{

    public function actionIndex(int $page = 1)
    {
        $posts = Post::getAllPosts($page);
        if ($posts) {
            include_once ROOT . '/views/posts/index.php';
        } else {
            header("HTTP/1.0 404 Not Found");
        }
    }

    public function actionShow( int $id)
    {
        $post = Post::getPostById($id);
        if (!$post) {
            header("HTTP/1.0 404 Not Found");
        } else {
            include_once ROOT . '/views/posts/show.php';
        }
    }

    public function actionStore()
    {
        if (isset($_POST['save_post'])) {
            $author = addslashes(htmlspecialchars($_POST['author_name']));
            $title = addslashes(htmlspecialchars($_POST['title']));
            $text = addslashes(htmlspecialchars($_POST['text']));
            Post::createNewPost($author, $title, $text);
            header('Location:/');
        }
    }

    public function actionComment()
    {
        if (isset($_POST['save_comment'])) {
            $message = addslashes(htmlspecialchars($_POST['message']));
            $authorName = addslashes(htmlspecialchars($_POST['author_name']));
            $postId = addslashes(htmlspecialchars($_POST['post_id']));
            Post::saveComment($message, $authorName, $postId);
            header('Location:/post/' . $postId);
        }
    }


}