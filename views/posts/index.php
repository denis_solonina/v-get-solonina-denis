<!DOCTYPE HTML>
<html>
<head>
    <title>Blog V-get</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>

<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <link rel="stylesheet" href="/main.css"/>
    <link rel="stylesheet" href="/css/bootstrap.css">
</head>
<body>


<div id="main">

    <?php if (!empty($posts['popular_post'])) { ?>
        <div class="sld-main">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="text-sld">
                            <p>
                                <?php echo $posts['popular_post'][0]['title']; ?>

                            <p>
                                Author: <?php echo $posts['popular_post'][0]['author_name']; ?> </p>
                            Comments: <?php echo $posts['popular_post'][0]['com']; ?> </p>
                            Preview: <?php echo $posts['popular_post'][0]['short_text']; ?> </p>
                            <p>
                                <a href="<?php echo '/post/' . $posts['popular_post'][0]['id']; ?>">
                                    <button type="button" class="btn btn-primary">Show</button>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                    unset($posts['popular_post'][0]);
                    foreach ($posts['popular_post'] as $post) {
                        ?>
                        <div class="carousel-item">
                            <div class="text-sld">

                                <?php echo $post['title']; ?>

                                <p>
                                    <?php echo $post['text']; ?>
                                </p>
                                <p>
                                    Author: <?php echo $post['author_name']; ?> </p>
                                Comments: <?php echo $post['com']; ?> </p>
                                Preview: <?php echo $post['short_text']; ?> </p>
                                <a href="<?php echo '/post/' . $post['id']; ?>">
                                    <button type="button" class="btn btn-primary">Show</button>
                                </a>
                                </p>
                            </div>

                        </div>
                    <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
   <?php } ?>


<div class="container">

    <form action="/post-create" method="post">
        <div class="form-group">
            <label for="exampleFormControlInput1">Author Name</label>
            <input type="text" class="form-control" id="author_name" name="author_name" placeholder="Name" required>
        </div>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
        </div>
        <div class="form-group">
            <label for="Textarea1">Post`s text</label>
            <textarea class="form-control"  name="text" id="Textarea1" rows="4" required></textarea>
        </div>
        <div class="form-group">
            <input type="submit" name="save_post" value="Save" class="btn btn-info">

        </div>

    </form>
</div>
    <main role="main" class="container">
    <?php foreach ($posts['other_post'] as $post) { ?>

        <div class="jumbotron">
            <h1> <?php echo $post['title']; ?> </h1>
            <h5> Author: <?php echo $post['author_name']; ?> </h5>
            <h5> Date:  <?php echo $post['date']; ?> </h5>
            <h5> Comments:  <?php echo $post['com']; ?> </h5>
            <h4><?php echo $post['short_text'] ?></h4>
            <a class="btn btn-lg btn-primary" href="<?php echo '/post/'.$post['id']; ?>" role="button"> View post &raquo;</a>
        </div>

<?php } ?>
    </main>
</div>
<!-- Footer -->
<footer id="footer">


</footer>


<!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"
        type="text/javascript"><!--mce:0--></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<nav aria-label="...">
    <ul class="pagination">
        <li class="page-item  <?php if (!$posts['paginate']['prev_page']) echo ' disabled' ;?>">
            <a class="page-link" href="/posts/page=<?php echo $posts['paginate']['prev_page'] ?>" tabindex="-1">Previous</a>
        </li>
        <li class="page-item"  <?php if (!$posts['paginate']['prev_page']) echo ' hidden' ;?>><a class="page-link" href="/posts/page=<?php if (!$posts['paginate']['prev_page']) {echo $posts['paginate']['page'];} else {echo $posts['paginate']['prev_page'];} ?>"><?php echo $posts['paginate']['prev_page'] ?></a></li>
        <li class="page-item active">
            <a class="page-link" href="/posts/page=<?php echo $posts['paginate']['page'] ?>"><?php echo $posts['paginate']['page'] ?><span class="sr-only">(current)</span></a>
        </li>
        <li class="page-item"  <?php if (!$posts['paginate']['next_page']) echo ' hidden' ;?>><a class="page-link" href="/posts/page=<?php echo $posts['paginate']['next_page'] ?>"><?php echo $posts['paginate']['next_page'] ?></a></li>
        <li class="page-item"  <?php if (!$posts['paginate']['next_page']) echo ' hidden' ;?>>
            <a class="page-link" href="/posts/page=<?php echo $posts['paginate']['next_page'] ?>">Next</a>
        </li>
    </ul>
</nav>

</body>

</html>


<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 19.10.18
 * Time: 22:54
 */