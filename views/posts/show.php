<!DOCTYPE HTML>
<html>
<head>
    <title>Blog V-get</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <link rel="stylesheet" href="main.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>


<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3"><?php echo $post['post']['title'] ?></h1>
            <p><?php echo $post['post']['text']?></p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
        <?php foreach ($post['comments'] as $comment) { ?>

            <div class="col-md-4">
                <h2><?php echo $comment['author_comment'];?></h2>
                <h6 ><?php echo $comment['date'];?></h6>
                <h3><?php echo $comment  ['message'];?></h3>
                <hr>
            </div>

            <?php } ?>
        </div>


            <form action="save-comment" method="post">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Author Name</label>
                    <input type="text" class="form-control" id="author_name" name="author_name" placeholder="Name" required>
                </div>
                <div class="form-group">
                    <label for="title">Message</label>
                    <input type="text" class="form-control" id="message" name="message" placeholder="Message" required>
                </div>
                <div class="form-group">
                    <input type="submit" name="save_comment" value="Leave comment" class="btn btn-info">
                    <input type="text" name="post_id" value="<?php echo $post['post']['id'];?>" class="btn btn-info" hidden>

                </div>

            </form>




    </div> <!-- /container -->

</main>






<!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"
        type="text/javascript"><!--mce:0--></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</body>

</html>